"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var ProductJobService = (function () {
    function ProductJobService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.url = 'http://localhost:8000/jobs/jobproducts.json'; // URL to web api
    }
    ProductJobService.prototype.getProductJobs = function () {
        return this.http.get(this.url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProductJobService.prototype.getProductJob = function (id) {
        return this.getProductJobs()
            .then(function (productJobs) { return productJobs.find(function (productJob) { return productJob.id === id; }); });
    };
    ProductJobService.prototype.getProductJobsByJob = function (jobid) {
        return this.getProductJobs()
            .then(function (productJobs) { return productJobs.filter(function (productJob) { return productJob.job === jobid; }); });
    };
    ProductJobService.prototype.delete = function (id) {
        var url = "http://localhost:8000/jobs/jobproducts/" + id;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    ProductJobService.prototype.create = function (productJob) {
        return this.http.post('http://localhost:8000/jobs/jobproducts', JSON.stringify(productJob), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ProductJobService.prototype.update = function (productJob) {
        var url = "http://localhost:8000/jobs/jobproducts/" + productJob.id;
        return this.http
            .put(url, JSON.stringify(productJob), { headers: this.headers })
            .toPromise()
            .then(function () { return productJob; })
            .catch(this.handleError);
    };
    ProductJobService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    ProductJobService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ProductJobService);
    return ProductJobService;
}());
exports.ProductJobService = ProductJobService;
//# sourceMappingURL=productjob.service.js.map