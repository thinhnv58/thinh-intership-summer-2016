"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var Observable_1 = require('rxjs/Observable');
var Subject_1 = require('rxjs/Subject');
var job_1 = require('./job');
var job_service_1 = require('./job.service');
var customer_search_service_1 = require('../customer/customer-search.service');
var customer_service_1 = require('../customer/customer.service');
var car_search_service_1 = require('../car/car-search.service');
var car_service_1 = require('../car/car.service');
var productcatalog_1 = require('../product/productcatalog');
var product_service_1 = require('../product/product.service');
var productcatalog_service_1 = require('../product/productcatalog.service');
var servicemain_service_1 = require('./servicemain.service');
var servicejob_1 = require('./servicejob');
var servicejob_service_1 = require('./servicejob.service');
var productjob_1 = require('./productjob');
var productjob_service_1 = require('./productjob.service');
var TypePC = (function () {
    function TypePC() {
    }
    return TypePC;
}());
var JobDetailComponent = (function () {
    function JobDetailComponent(jobService, route, location, customerSearchService, carSearchService, customerService, carService, serviceMainService, serviceJobService, productService, productCatalogService, productJobService) {
        this.jobService = jobService;
        this.route = route;
        this.location = location;
        this.customerSearchService = customerSearchService;
        this.carSearchService = carSearchService;
        this.customerService = customerService;
        this.carService = carService;
        this.serviceMainService = serviceMainService;
        this.serviceJobService = serviceJobService;
        this.productService = productService;
        this.productCatalogService = productCatalogService;
        this.productJobService = productJobService;
        this.job = new job_1.Job();
        this.searchTermsCustomer = new Subject_1.Subject();
        this.searchTermsCar = new Subject_1.Subject();
        this.serviceJob = new servicejob_1.ServiceJob();
        this.productCatalog = new productcatalog_1.ProductCatalog();
        this.productJob = new productjob_1.ProductJob();
        this.typePC = new TypePC();
    }
    JobDetailComponent.prototype.searchCustomer = function (term) {
        this.searchTermsCustomer.next(term);
    };
    JobDetailComponent.prototype.searchCar = function (term) {
        this.searchTermsCar.next(term);
    };
    JobDetailComponent.prototype.selectCustomer = function (customer) {
        this.selectedCustomer = customer;
        console.log(this.selectedCustomer.first_name);
    };
    JobDetailComponent.prototype.selectCar = function (car) {
        this.selectedCar = car;
        console.log(this.selectedCar.license_plate);
    };
    JobDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var id = +params['id'];
            if (!isNaN(id)) {
                _this.jobService.getJob(id)
                    .then(function (job) {
                    _this.job = job;
                    _this.customerService.getCustomer(_this.job.customer)
                        .then(function (customer) { return _this.selectedCustomer = customer; });
                    _this.carService.getCar(_this.job.car)
                        .then(function (car) { return _this.selectedCar = car; });
                    _this.serviceJobService.getServiceJobsByJob(_this.job.id)
                        .then(function (serviceJobs) { return _this.serviceJobs = serviceJobs; });
                    _this.productJobService.getProductJobsByJob(+_this.job.id)
                        .then(function (productJobs) {
                        _this.productJobs = productJobs;
                        var _loop_1 = function(i) {
                            _this.productService.getProduct(+_this.productJobs[i].product)
                                .then(function (product) {
                                _this.productJobs[i].product_name = product.name;
                                _this.productJobs[i].product_cost = product.cost;
                                _this.productJobs[i].product_photo = product.photo;
                            });
                        };
                        for (var i = 0; i < _this.productJobs.length; i++) {
                            _loop_1(i);
                        }
                    });
                });
            }
        });
        /*------ Customer search service ----*/
        this.customers = this.searchTermsCustomer
            .debounceTime(300) // wait for 300ms pause in events
            .distinctUntilChanged() // ignore if next search term is same as previous
            .switchMap(function (term) { return term // switch to new observable each time
            ? _this.customerSearchService.search(term)
            : Observable_1.Observable.of([]); })
            .catch(function (error) {
            // TODO: real error handling
            console.log(error);
            return Observable_1.Observable.of([]);
        });
        /*---- Car search ----*/
        this.cars = this.searchTermsCar
            .debounceTime(300) // wait for 300ms pause in events
            .distinctUntilChanged() // ignore if next search term is same as previous
            .switchMap(function (term) { return term // switch to new observable each time
            ? _this.carSearchService.search(term)
            : Observable_1.Observable.of([]); })
            .catch(function (error) {
            // TODO: real error handling
            console.log(error);
            return Observable_1.Observable.of([]);
        });
        this.serviceMainService.getServiceMains()
            .then(function (serviceMains) { return _this.serviceMains = serviceMains; });
        this.typePCs = [
            { type: 'P', name: 'Performance and Path' },
            { type: 'I', name: 'Interior Accessories' },
            { type: 'E', name: 'Exterior Accessories' },
            { type: 'O', name: 'Other Accessories' }
        ];
    };
    JobDetailComponent.prototype.save = function () {
        var _this = this;
        this.jobService.update(this.job)
            .then(function () { return _this.goBack(); });
    };
    JobDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    JobDetailComponent.prototype.createJob = function () {
        this.job.customer = this.selectedCustomer.id;
        this.job.car = this.selectedCar.id;
        this.jobService.create(this.job);
    };
    JobDetailComponent.prototype.updateJob = function () {
        var _this = this;
        this.job.customer = this.selectedCustomer.id;
        this.job.car = this.selectedCar.id;
        this.jobService.update(this.job)
            .then(function () { return _this.goBack(); });
    };
    JobDetailComponent.prototype.addService = function (serviceName, serviceCost) {
        var _this = this;
        this.serviceJob.name = serviceName;
        this.serviceJob.cost = serviceCost;
        this.serviceJob.job = +this.job.id;
        this.serviceJobService.create(this.serviceJob)
            .then(function () {
            _this.serviceJobs.push(_this.serviceJob);
            console.log(_this.serviceJobs);
        });
    };
    JobDetailComponent.prototype.changeProductType = function () {
        var _this = this;
        this.productCatalogService.getProductCatalogsByType(this.typePC.type)
            .then(function (productCatalogs) { return _this.productCatalogs = productCatalogs; });
    };
    JobDetailComponent.prototype.updateProducts = function () {
        var _this = this;
        console.log(this.productCatalog.id);
        this.productService.getProductsByCatalog(+this.productCatalog.id)
            .then(function (products) { return _this.products = products; });
        /*console.log(this.products.length);*/
    };
    JobDetailComponent.prototype.createProductJob = function (product) {
        var _this = this;
        this.productJob.product = +product.id;
        this.productJob.job = +this.job.id;
        this.productJob.quantum = 1;
        this.productJobService.create(this.productJob)
            .then(function () {
            _this.productService.getProduct(+_this.productJob.product)
                .then(function (product) {
                _this.productJob.product_name = product.name;
                _this.productJob.product_cost = product.cost;
                _this.productJob.product_photo = product.photo;
            });
            _this.productJobs.push(_this.productJob);
        });
    };
    JobDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-job-detail',
            templateUrl: 'js/app/job/job-detail.component.html',
            providers: [
                customer_search_service_1.CustomerSearchService,
                car_search_service_1.CarSearchService,
                customer_service_1.CustomerService,
                car_service_1.CarService,
                servicemain_service_1.ServiceMainService,
                servicejob_service_1.ServiceJobService,
                product_service_1.ProductService,
                productcatalog_service_1.ProductCatalogService,
                productjob_service_1.ProductJobService
            ]
        }), 
        __metadata('design:paramtypes', [job_service_1.JobService, router_1.ActivatedRoute, common_1.Location, customer_search_service_1.CustomerSearchService, car_search_service_1.CarSearchService, customer_service_1.CustomerService, car_service_1.CarService, servicemain_service_1.ServiceMainService, servicejob_service_1.ServiceJobService, product_service_1.ProductService, productcatalog_service_1.ProductCatalogService, productjob_service_1.ProductJobService])
    ], JobDetailComponent);
    return JobDetailComponent;
}());
exports.JobDetailComponent = JobDetailComponent;
//# sourceMappingURL=job-detail.component.js.map