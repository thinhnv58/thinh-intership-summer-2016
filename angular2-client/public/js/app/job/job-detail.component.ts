import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location }               from '@angular/common';
import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';
import { Job }        from './job';
import { JobService } from './job.service';
import { CustomerSearchService } from '../customer/customer-search.service';
import { Customer } from '../customer/customer';
import { CustomerService } from '../customer/customer.service';
import { Car } from '../car/car';
import { CarSearchService } from '../car/car-search.service';
import { CarService } from '../car/car.service';
import { Product } from '../product/product';
import { ProductCatalog } from '../product/productcatalog';
import { ProductService } from '../product/product.service';
import { ProductCatalogService } from '../product/productcatalog.service';


import { ServiceMainService } from './servicemain.service';
import { ServiceMain } from './servicemain';
import { ServiceJob } from './servicejob';
import { ServiceJobService } from './servicejob.service';
import { ProductJob } from './productjob';
import { ProductJobService } from './productjob.service';

class TypePC{
  type: string;
  name: string;
}

@Component({
  selector: 'my-job-detail',
  templateUrl: 'js/app/job/job-detail.component.html',
  providers: [ 
    CustomerSearchService, 
    CarSearchService,
    CustomerService,
    CarService,
    ServiceMainService,
    ServiceJobService,
    ProductService,
    ProductCatalogService,
    ProductJobService
    ]

})
export class JobDetailComponent implements OnInit {
  job = new Job();
  
  customers: Observable<Customer[]>;
  cars: Observable<Car[]>;
  
  selectedCustomer: Customer;
  selectedCar: Car;
  

  
  private searchTermsCustomer = new Subject<string>();
  private searchTermsCar = new Subject<string>();
  
  serviceMains: ServiceMain[];
  serviceJobs: ServiceJob[];
  serviceJob = new ServiceJob();
  productCatalogs: ProductCatalog[];
  productCatalog = new ProductCatalog();
  products: Product[];
  productJobs: ProductJob[];
  productJob = new ProductJob();
  
  typePCs: TypePC[];
  typePC = new TypePC();

  constructor(
    private jobService: JobService,
    private route: ActivatedRoute,
    private location: Location,
    private customerSearchService: CustomerSearchService,
    private carSearchService: CarSearchService,
    private customerService: CustomerService,
    private carService: CarService,
    private serviceMainService: ServiceMainService,
    private serviceJobService: ServiceJobService, 
    private productService: ProductService,
    private productCatalogService: ProductCatalogService,
    private productJobService: ProductJobService    
  ) {}

  searchCustomer(term: string): void {
    this.searchTermsCustomer.next(term);
  }
  searchCar(term: string): void {
    this.searchTermsCar.next(term);
  }
  
  selectCustomer(customer: Customer): void{
    this.selectedCustomer = customer;
    console.log(this.selectedCustomer.first_name)

  }
  selectCar(car: Car): void{
    this.selectedCar = car;
    console.log(this.selectedCar.license_plate)

  }
  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      if (!isNaN(id)){
        this.jobService.getJob(id)
        .then(job => {
          this.job = job;
          this.customerService.getCustomer(this.job.customer)
          .then(customer => this.selectedCustomer = customer);
          this.carService.getCar(this.job.car)
          .then(car => this.selectedCar = car);

          this.serviceJobService.getServiceJobsByJob(this.job.id)
          .then(serviceJobs => this.serviceJobs = serviceJobs);

          this.productJobService.getProductJobsByJob(+this.job.id)
          .then(productJobs =>{ 
          this.productJobs = productJobs;
          for (let i=0; i<this.productJobs.length; i++){
            this.productService.getProduct(+this.productJobs[i].product)
            .then(product => {
              this.productJobs[i].product_name = product.name;
              this.productJobs[i].product_cost = product.cost;
              this.productJobs[i].product_photo = product.photo;
              
            });
            
          }

          });

          

      });

    }
      
      
    });




    /*------ Customer search service ----*/
    this.customers = this.searchTermsCustomer
      .debounceTime(300)        // wait for 300ms pause in events
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time
        // return the http search observable
        ? this.customerSearchService.search(term)
        // or the observable of empty heroes if no search term
        : Observable.of<Customer[]>([]))
      .catch(error => {
        // TODO: real error handling
        console.log(error);
        return Observable.of<Customer[]>([]);
      });
    /*---- Car search ----*/
      this.cars = this.searchTermsCar
      .debounceTime(300)        // wait for 300ms pause in events
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time
        // return the http search observable
        ? this.carSearchService.search(term)
        // or the observable of empty heroes if no search term
        : Observable.of<Car[]>([]))
      .catch(error => {
        // TODO: real error handling
        console.log(error);
        return Observable.of<Car[]>([]);
      });

    this.serviceMainService.getServiceMains()
      .then(serviceMains => this.serviceMains = serviceMains);
    
    this.typePCs = [
      {type: 'P', name: 'Performance and Path'}, 
      {type: 'I', name: 'Interior Accessories'},
      {type: 'E', name: 'Exterior Accessories'},
      {type: 'O', name: 'Other Accessories'}
      
      ];

    
      
  }
  save(): void {
    this.jobService.update(this.job)
      .then(() => this.goBack());
  }
  goBack(): void {
    this.location.back();
  }

  createJob(): void{
    this.job.customer = this.selectedCustomer.id;
    this.job.car = this.selectedCar.id;
    this.jobService.create(this.job);

  }

  updateJob(): void {
    this.job.customer = this.selectedCustomer.id;
    this.job.car = this.selectedCar.id;
    this.jobService.update(this.job)
      .then(() => this.goBack());
  }

  addService(serviceName: string, serviceCost): void{
    this.serviceJob.name = serviceName;
    this.serviceJob.cost = serviceCost;
    this.serviceJob.job = +this.job.id;
    
    this.serviceJobService.create(this.serviceJob)
    .then(() => {
      this.serviceJobs.push(this.serviceJob);
      console.log(this.serviceJobs)
    });
  }


  changeProductType(): void{
    this.productCatalogService.getProductCatalogsByType(this.typePC.type)
    .then(productCatalogs => this.productCatalogs = productCatalogs);


  }
  updateProducts(): void{
    console.log(this.productCatalog.id);
    this.productService.getProductsByCatalog(+this.productCatalog.id)
    .then(products => this.products = products);

    /*console.log(this.products.length);*/
  }
  createProductJob(product: Product): void{
    this.productJob.product = +product.id;
    this.productJob.job = +this.job.id;
    this.productJob.quantum = 1;

    this.productJobService.create(this.productJob)
    .then(() => {
      this.productService.getProduct(+this.productJob.product)
            .then(product => {
              this.productJob.product_name = product.name;
              this.productJob.product_cost = product.cost;
              this.productJob.product_photo = product.photo;
              
            });
      this.productJobs.push(this.productJob);

    });
  }

}
