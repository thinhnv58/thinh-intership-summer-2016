export class ProductJob{
    id: number;
    job: number;
    product: number;
    quantum: number;
    
    product_name: string;
    product_cost: string;
    product_photo: string;
}