"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var ServiceJobService = (function () {
    function ServiceJobService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.url = 'http://localhost:8000/jobs/jobservices.json'; // URL to web api
    }
    ServiceJobService.prototype.getServiceJobs = function () {
        return this.http.get(this.url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ServiceJobService.prototype.getServiceJob = function (id) {
        return this.getServiceJobs()
            .then(function (serviceJobs) { return serviceJobs.find(function (serviceJob) { return serviceJob.id === id; }); });
    };
    ServiceJobService.prototype.getServiceJobsByJob = function (jobid) {
        return this.getServiceJobs()
            .then(function (serviceJobs) { return serviceJobs.filter(function (serviceJob) { return serviceJob.job === jobid; }); });
    };
    ServiceJobService.prototype.delete = function (id) {
        var url = "http://localhost:8000/jobs/jobservices/" + id;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    ServiceJobService.prototype.create = function (serviceJob) {
        return this.http.post('http://localhost:8000/jobs/jobservices', JSON.stringify(serviceJob), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ServiceJobService.prototype.update = function (serviceJob) {
        var url = "http://localhost:8000/jobs/jobservices/" + serviceJob.id;
        return this.http
            .put(url, JSON.stringify(serviceJob), { headers: this.headers })
            .toPromise()
            .then(function () { return serviceJob; })
            .catch(this.handleError);
    };
    ServiceJobService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    ServiceJobService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ServiceJobService);
    return ServiceJobService;
}());
exports.ServiceJobService = ServiceJobService;
//# sourceMappingURL=servicejob.service.js.map