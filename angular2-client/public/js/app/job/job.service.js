"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var JobService = (function () {
    function JobService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.url = 'http://localhost:8000/jobs/.json'; // URL to web api
    }
    JobService.prototype.getJobs = function () {
        return this.http.get(this.url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    JobService.prototype.getJob = function (id) {
        return this.getJobs()
            .then(function (jobs) { return jobs.find(function (job) { return job.id === id; }); });
    };
    JobService.prototype.delete = function (id) {
        var url = "http://localhost:8000/jobs/" + id;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    /*  create(customer: number, car: number, problems: string,
        due_from: string, due_to:string, cost: string, draft: boolean,
        advisor: number, note: string, team_repair: number, status: string): Promise<Job> {
          console.log(JSON.stringify({customer: customer, car: car, problems: problems, due_from: due_from,
              due_to: due_to, cost: cost, draft: draft, advisor: advisor, note: note, team_repair: team_repair, status: status }))
        return this.http
          .post(this.url, JSON.stringify({customer: customer, car: car, problems: problems, due_from: due_from,
              due_to: due_to, cost: cost, draft: draft, advisor: advisor, note: note, team_repair: team_repair, status: status }),
              {headers: this.headers})
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
      }*/
    JobService.prototype.create = function (job) {
        return this.http.post('http://localhost:8000/jobs/', JSON.stringify(job), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    JobService.prototype.update = function (job) {
        var url = "http://localhost:8000/jobs/" + job.id;
        return this.http
            .put(url, JSON.stringify(job), { headers: this.headers })
            .toPromise()
            .then(function () { return job; })
            .catch(this.handleError);
    };
    JobService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    JobService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], JobService);
    return JobService;
}());
exports.JobService = JobService;
//# sourceMappingURL=job.service.js.map