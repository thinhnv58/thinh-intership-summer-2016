import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Job } from './job';
@Injectable()
export class JobService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/jobs/.json';  // URL to web api
  constructor(private http: Http) { }
  getJobs(): Promise<Job[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as Job[])
               .catch(this.handleError);
  }
  getJob(id: number): Promise<Job> {
    return this.getJobs()
               .then(jobs => jobs.find(job => job.id === id));
  }
  delete(id: number): Promise<void> {
    const url = `http://localhost:8000/jobs/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
/*  create(customer: number, car: number, problems: string, 
    due_from: string, due_to:string, cost: string, draft: boolean, 
    advisor: number, note: string, team_repair: number, status: string): Promise<Job> {
      console.log(JSON.stringify({customer: customer, car: car, problems: problems, due_from: due_from,
          due_to: due_to, cost: cost, draft: draft, advisor: advisor, note: note, team_repair: team_repair, status: status }))
    return this.http
      .post(this.url, JSON.stringify({customer: customer, car: car, problems: problems, due_from: due_from,
          due_to: due_to, cost: cost, draft: draft, advisor: advisor, note: note, team_repair: team_repair, status: status }), 
          {headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }*/

  create(job: Job): Promise<Job> {
    return this.http.post('http://localhost:8000/jobs/', JSON.stringify(job), {headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }
  
  update(job: Job): Promise<Job> {
    const url = `http://localhost:8000/jobs/${job.id}`;
    return this.http
      .put(url, JSON.stringify(job), {headers: this.headers})
      .toPromise()
      .then(() => job)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}


