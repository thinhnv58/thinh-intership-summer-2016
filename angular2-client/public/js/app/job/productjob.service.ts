import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ProductJob } from './productjob';
@Injectable()
export class ProductJobService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/jobs/jobproducts.json';  // URL to web api
  constructor(private http: Http) { }
  getProductJobs(): Promise<ProductJob[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as ProductJob[])
               .catch(this.handleError);
  }
  getProductJob(id: number): Promise<ProductJob> {
    return this.getProductJobs()
               .then(productJobs => productJobs.find(productJob => productJob.id === id));
  }

  getProductJobsByJob(jobid: number): Promise<ProductJob[]> {
    return this.getProductJobs()
               .then(productJobs => productJobs.filter(productJob => productJob.job === jobid));
  }


  delete(id: number): Promise<void> {
    const url = `http://localhost:8000/jobs/jobproducts/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(productJob: ProductJob): Promise<ProductJob> {
    return this.http.post('http://localhost:8000/jobs/jobproducts', JSON.stringify(productJob), {headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }
  
  update(productJob: ProductJob): Promise<ProductJob> {
    const url = `http://localhost:8000/jobs/jobproducts/${productJob.id}`;
    return this.http
      .put(url, JSON.stringify(productJob), {headers: this.headers})
      .toPromise()
      .then(() => productJob)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
