export class EventJob{
    id: number;
    created: string;
    updated: string;
    text: string;
    job: number;
}