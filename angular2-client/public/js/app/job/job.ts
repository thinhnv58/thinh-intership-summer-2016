export class Job{
    id: number;
    created: string;
    updated: string;
    customer: number;
    car: number;
    problems: string;
    due_from: string;
    due_to: string;
    cost: string;
    draft: boolean;
    advisor: number;
    note: string;
    team_repair: number;
    status: string;

    customer_info: string;
    car_info: string;
    car_model: string;
    car_photo: string;
}





