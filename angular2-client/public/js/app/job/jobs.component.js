"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var job_service_1 = require('./job.service');
var customer_service_1 = require('../customer/customer.service');
var car_service_1 = require('../car/car.service');
var carmodel_service_1 = require('../car/carmodel.service');
var JobsComponent = (function () {
    function JobsComponent(jobService, carService, customerService, carModelService) {
        this.jobService = jobService;
        this.carService = carService;
        this.customerService = customerService;
        this.carModelService = carModelService;
    }
    JobsComponent.prototype.getJobs = function () {
        var _this = this;
        this.jobService
            .getJobs()
            .then(function (jobs) {
            _this.jobs = jobs;
            var _loop_1 = function(i) {
                _this.customerService.getCustomer(jobs[i].customer)
                    .then(function (customer) {
                    jobs[i].customer_info = customer.first_name + ' ' + customer.last_name + ' - ' + customer.phone;
                });
                _this.carService.getCar(jobs[i].car)
                    .then(function (car) {
                    _this.carModelService.getCarModel(car.carModel)
                        .then(function (carModel) {
                        jobs[i].car_info = car.license_plate + ' ' + carModel.name;
                        jobs[i].car_photo = carModel.photo;
                    });
                });
            };
            for (var i = 0; i < jobs.length; i++) {
                _loop_1(i);
            }
        });
    };
    /*  add(customer: number, car: number, problems: string,
        due_from: string, due_to:string, cost: string, draft: boolean,
        advisor: number, note: string, team_repair: number, status: string ): void {
    
        this.jobService.create(customer, car, problems,
        due_from, due_to, cost, draft,
        advisor, note, team_repair, status)
          .then(job => {
            this.jobs.push(job);
            this.selectedJob = null;
          });
      }*/
    /*  delete(job: Job): void {
        this.jobService
            .delete(job.id)
            .then(() => {
              this.jobs = this.jobs.filter(j => j !== job);
              if (this.selectedJob === job) { this.selectedJob = null; }
            });
      }*/
    JobsComponent.prototype.ngOnInit = function () {
        this.getJobs();
    };
    JobsComponent = __decorate([
        core_1.Component({
            selector: 'my-jobs',
            templateUrl: 'js/app/job/jobs.component.html',
            providers: [carmodel_service_1.CarModelService]
        }), 
        __metadata('design:paramtypes', [job_service_1.JobService, car_service_1.CarService, customer_service_1.CustomerService, carmodel_service_1.CarModelService])
    ], JobsComponent);
    return JobsComponent;
}());
exports.JobsComponent = JobsComponent;
//# sourceMappingURL=jobs.component.js.map