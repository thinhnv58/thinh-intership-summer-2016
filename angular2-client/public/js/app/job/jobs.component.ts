import { Component, OnInit } from '@angular/core';


import { Job }        from './job';
import { JobService } from './job.service';
import { Customer }   from '../customer/customer';
import { Car }        from '../car/car';
import { CustomerService } from '../customer/customer.service';
import { CarService } from '../car/car.service';
import { CarModelService } from '../car/carmodel.service';
import { CarModel } from '../car/carmodel';


@Component({

  selector: 'my-jobs',
  templateUrl: 'js/app/job/jobs.component.html',

  providers: [CarModelService]
})
export class JobsComponent implements OnInit {
  jobs: Job[];

  selectedJob: Job;
  constructor(
    private jobService: JobService,
    private carService: CarService,
    private customerService: CustomerService,
    private carModelService: CarModelService
    ) {}
  
  getJobs(): void {
    this.jobService
        .getJobs()
        .then(jobs => {
          this.jobs = jobs;
          for (let i=0; i< jobs.length; i++) {
            this.customerService.getCustomer( jobs[i].customer )
            .then(customer => {
              jobs[i].customer_info = customer.first_name + ' ' + customer.last_name + ' - ' + customer.phone ;
            })

            this.carService.getCar( jobs[i].car )
            .then(car => {
              this.carModelService.getCarModel(car.carModel)
              .then(carModel => {
                jobs[i].car_info = car.license_plate + ' ' +  carModel.name;
                jobs[i].car_photo = carModel.photo;
                
              })
              
              
            });

            
          }
          

    });
  }
/*  add(customer: number, car: number, problems: string, 
    due_from: string, due_to:string, cost: string, draft: boolean, 
    advisor: number, note: string, team_repair: number, status: string ): void {

    this.jobService.create(customer, car, problems, 
    due_from, due_to, cost, draft, 
    advisor, note, team_repair, status)
      .then(job => {
        this.jobs.push(job);
        this.selectedJob = null;
      });
  }*/
/*  delete(job: Job): void {
    this.jobService
        .delete(job.id)
        .then(() => {
          this.jobs = this.jobs.filter(j => j !== job);
          if (this.selectedJob === job) { this.selectedJob = null; }
        });
  }*/
  ngOnInit(): void {
    this.getJobs();
  }
/*  }
  onSelect(job: Job): void {
    this.selectedJob = job;
  }
*/
}
