import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ServiceJob } from './servicejob';
@Injectable()
export class ServiceJobService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/jobs/jobservices.json';  // URL to web api
  constructor(private http: Http) { }
  getServiceJobs(): Promise<ServiceJob[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as ServiceJob[])
               .catch(this.handleError);
  }
  getServiceJob(id: number): Promise<ServiceJob> {
    return this.getServiceJobs()
               .then(serviceJobs => serviceJobs.find(serviceJob => serviceJob.id === id));
  }

  getServiceJobsByJob(jobid: number): Promise<ServiceJob[]> {
    return this.getServiceJobs()
               .then(serviceJobs => serviceJobs.filter(serviceJob => serviceJob.job === jobid));
  }


  delete(id: number): Promise<void> {
    const url = `http://localhost:8000/jobs/jobservices/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(serviceJob: ServiceJob): Promise<ServiceJob> {
    return this.http.post('http://localhost:8000/jobs/jobservices', JSON.stringify(serviceJob), {headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }
  
  update(serviceJob: ServiceJob): Promise<ServiceJob> {
    const url = `http://localhost:8000/jobs/jobservices/${serviceJob.id}`;
    return this.http
      .put(url, JSON.stringify(serviceJob), {headers: this.headers})
      .toPromise()
      .then(() => serviceJob)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
