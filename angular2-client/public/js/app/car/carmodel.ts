export class CarModel{
    id: number;
    name: string;
    make: number;
    type: string;
    photo: string;
    description: string;
}
