import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location }               from '@angular/common';
import { Car }        from './car';
import { CarService } from './car.service';
import { CarMake } from './carmake';
import { CarModel } from './carmodel';
import { CarMakeService } from './carmake.service';
import { CarModelService } from './carmodel.service';



@Component({
  selector: 'my-car-detail',
  templateUrl: 'js/app/car/car-detail.component.html',
  providers: [ CarMakeService, CarModelService ]  
})
export class CarDetailComponent implements OnInit {
  car = new Car();
  carModel = new CarModel();
  carMake = new CarMake();
  carMakes: CarMake[];
  carModels: CarModel[];

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private carMakeService: CarMakeService,
    private carService: CarService,
    private carModelService: CarModelService
  ) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      if ( !isNaN(id)){
        this.carService.getCar(id)
          .then(car => {

            this.car = car;
            console.log(this.car.license_plate);

            this.carModelService.getCarModel(this.car.carModel)
            .then(carModel => {
              this.carModel = carModel;
              console.log(this.carModel.name);
              
              this.carMakeService.getCarMake(this.carModel.make)
              .then(carMake => {
                this.carMake = carMake;
                console.log(this.carMake.name);

                this.carModelService.getCarModelListByMake(carMake.id)
                .then(carModels => this.carModels = carModels);

              });
            });
          });
       
      }
      else{
        this.carMake.id = 1;
        this.carModelService.getCarModelListByMake(this.carMake.id)
                .then(carModels => this.carModels = carModels);
        
      }
        
    });
    
    this.carMakeService.getCarMakes()
    .then(carMakes => {
      this.carMakes = carMakes;
    });

  }
  

  updateCarModels(): void{
    this.carModelService.getCarModelListByMake(this.carMake.id)
                .then(carModels => this.carModels = carModels);
  }
  updateCarModel(): void{
    console.log(this.carModel.id);
    this.carModelService.getCarModel( +(this.carModel.id) )
    .then(carModel => {
      this.carModel = carModel;
    })
  }



/*  createCustomer(first_name, last_name, isMale, isCompany, organization, phone,
  mobile, email, address1, address2, town_or_city, note): void{
    this.customerService.create(first_name, last_name, isMale, isCompany, organization, phone,
  mobile, email, address1, address2, town_or_city, note);
    this.goBack();
  
  }*/
  updateCar(): void {
    this.car.carModel = this.carModel.id;
    this.carService.update(this.car)
      .then(() => this.goBack());
  }

  createCar(): void{
    this.car.carModel = this.carModel.id;
    this.carService.create(this.car);
  }

  goBack(): void {
    this.location.back();
  }
}
