import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Car } from './car';
@Injectable()
export class CarService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/cars/.json';  // URL to web api
  constructor(private http: Http) { }
  getCars(): Promise<Car[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as Car[])
               .catch(this.handleError);
  }
  getCar(id: number): Promise<Car> {
    return this.getCars()
               .then(cars => cars.find(car => car.id === id));
  }

/*  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
  */
 /* create(first_name: string, last_name: string, isMale: boolean, isCompany: boolean, organization: string, 
  phone: string, mobile: string, email: string, address1: string, address2: string, town_or_city: string, note: string ): Promise<Customer> {
    return this.http
      .post(this.url, JSON.stringify({first_name: first_name, last_name: last_name, isMale: isMale, isCompany: isCompany,
         organization: organization, phone: phone, mobile: mobile, email: email, address1: address1, address2: address2, 
         town_or_city: town_or_city, note: note}), {headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }
*/
  
  update(car: Car): Promise<Car> {
    const url = `http://localhost:8000/cars/${car.id}`;
    return this.http
      .put(url, JSON.stringify(car), {headers: this.headers})
      .toPromise()
      .then(() => car)
      .catch(this.handleError);
  }

  create(car: Car): Promise<Car> {
    return this.http.post('http://localhost:8000/cars/', JSON.stringify(car), {headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }
  
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
