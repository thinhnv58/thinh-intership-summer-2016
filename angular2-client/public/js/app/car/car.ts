export class Car{
    id: number;
    created: string;
    updated: string;
    license_plate: string;
    carModel: number;
    year: string;
    vin: string;
    horse_power: string;
    fuel: string;
    gear_box: string;
    engine: string;
    milekm_age: string;
    note: string;
}


