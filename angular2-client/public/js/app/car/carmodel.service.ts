import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { CarModel } from './carmodel';
@Injectable()
export class CarModelService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/cars/carmodels.json';  // URL to web api
  constructor(private http: Http) { }
  getCarModels(): Promise<CarModel[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as CarModel[])
               .catch(this.handleError);
  }

  getCarModelListByMake(id: number): Promise<CarModel[]>{
    return this.http.get(`http://localhost:8000/cars/modellistbymake/${id}`)
      .toPromise()
      .then(response => response.json() as CarModel[])
      .catch(this.handleError);
  }


  getCarModel(id: number): Promise<CarModel> {
    return this.getCarModels()
               .then(carModels => carModels.find(carModel => carModel.id === id));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
