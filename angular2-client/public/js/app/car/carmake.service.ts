import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { CarMake } from './carmake';
@Injectable()
export class CarMakeService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/cars/carmakes.json';  // URL to web api
  constructor(private http: Http) { }
  getCarMakes(): Promise<CarMake[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as CarMake[])
               .catch(this.handleError);
  }
  getCarMake(id: number): Promise<CarMake> {
    return this.getCarMakes()
               .then(carMakes => carMakes.find(carMake => carMake.id === id));
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
