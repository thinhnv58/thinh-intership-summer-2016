"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var car_1 = require('./car');
var car_service_1 = require('./car.service');
var carmake_1 = require('./carmake');
var carmodel_1 = require('./carmodel');
var carmake_service_1 = require('./carmake.service');
var carmodel_service_1 = require('./carmodel.service');
var CarDetailComponent = (function () {
    function CarDetailComponent(route, location, carMakeService, carService, carModelService) {
        this.route = route;
        this.location = location;
        this.carMakeService = carMakeService;
        this.carService = carService;
        this.carModelService = carModelService;
        this.car = new car_1.Car();
        this.carModel = new carmodel_1.CarModel();
        this.carMake = new carmake_1.CarMake();
    }
    CarDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var id = +params['id'];
            if (!isNaN(id)) {
                _this.carService.getCar(id)
                    .then(function (car) {
                    _this.car = car;
                    console.log(_this.car.license_plate);
                    _this.carModelService.getCarModel(_this.car.carModel)
                        .then(function (carModel) {
                        _this.carModel = carModel;
                        console.log(_this.carModel.name);
                        _this.carMakeService.getCarMake(_this.carModel.make)
                            .then(function (carMake) {
                            _this.carMake = carMake;
                            console.log(_this.carMake.name);
                            _this.carModelService.getCarModelListByMake(carMake.id)
                                .then(function (carModels) { return _this.carModels = carModels; });
                        });
                    });
                });
            }
            else {
                _this.carMake.id = 1;
                _this.carModelService.getCarModelListByMake(_this.carMake.id)
                    .then(function (carModels) { return _this.carModels = carModels; });
            }
        });
        this.carMakeService.getCarMakes()
            .then(function (carMakes) {
            _this.carMakes = carMakes;
        });
    };
    CarDetailComponent.prototype.updateCarModels = function () {
        var _this = this;
        this.carModelService.getCarModelListByMake(this.carMake.id)
            .then(function (carModels) { return _this.carModels = carModels; });
    };
    CarDetailComponent.prototype.updateCarModel = function () {
        var _this = this;
        console.log(this.carModel.id);
        this.carModelService.getCarModel(+(this.carModel.id))
            .then(function (carModel) {
            _this.carModel = carModel;
        });
    };
    /*  createCustomer(first_name, last_name, isMale, isCompany, organization, phone,
      mobile, email, address1, address2, town_or_city, note): void{
        this.customerService.create(first_name, last_name, isMale, isCompany, organization, phone,
      mobile, email, address1, address2, town_or_city, note);
        this.goBack();
      
      }*/
    CarDetailComponent.prototype.updateCar = function () {
        var _this = this;
        this.car.carModel = this.carModel.id;
        this.carService.update(this.car)
            .then(function () { return _this.goBack(); });
    };
    CarDetailComponent.prototype.createCar = function () {
        this.car.carModel = this.carModel.id;
        this.carService.create(this.car);
    };
    CarDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    CarDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-car-detail',
            templateUrl: 'js/app/car/car-detail.component.html',
            providers: [carmake_service_1.CarMakeService, carmodel_service_1.CarModelService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, common_1.Location, carmake_service_1.CarMakeService, car_service_1.CarService, carmodel_service_1.CarModelService])
    ], CarDetailComponent);
    return CarDetailComponent;
}());
exports.CarDetailComponent = CarDetailComponent;
//# sourceMappingURL=car-detail.component.js.map