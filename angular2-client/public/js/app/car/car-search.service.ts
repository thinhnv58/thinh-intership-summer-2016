import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { Car } from './car';
@Injectable()
export class CarSearchService {
  constructor(private http: Http) {}
  search(term: string): Observable<Car[]> {
    return this.http
               .get(`http://localhost:8000/cars/?search=${term}`)
               .map((r: Response) => r.json() as Car[]);
  }
}
