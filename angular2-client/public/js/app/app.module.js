"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
require('./rxjs-extensions');
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var http_1 = require('@angular/http');
var forms_1 = require('@angular/forms');
var app_component_1 = require('./app.component');
var jobs_component_1 = require('./job/jobs.component');
var job_detail_component_1 = require('./job/job-detail.component');
var job_service_1 = require('./job/job.service');
var customer_detail_component_1 = require('./customer/customer-detail.component');
var customer_service_1 = require('./customer/customer.service');
var customers_component_1 = require('./customer/customers.component');
var car_service_1 = require('./car/car.service');
var car_detail_component_1 = require('./car/car-detail.component');
var cars_component_1 = require('./car/cars.component');
var profile_component_1 = require('./user/profile.component');
var app_routing_module_1 = require('./app-routing.module');
var cookies_service_1 = require('angular2-cookie/services/cookies.service');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                app_routing_module_1.AppRoutingModule,
                forms_1.FormsModule,
            ],
            declarations: [
                app_component_1.AppComponent,
                jobs_component_1.JobsComponent,
                job_detail_component_1.JobDetailComponent,
                customer_detail_component_1.CustomerDetailComponent,
                customers_component_1.CustomersComponent,
                cars_component_1.CarsComponent,
                car_detail_component_1.CarDetailComponent,
                profile_component_1.ProfileComponent
            ],
            bootstrap: [app_component_1.AppComponent],
            providers: [
                job_service_1.JobService,
                customer_service_1.CustomerService,
                car_service_1.CarService,
                cookies_service_1.CookieService,
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map