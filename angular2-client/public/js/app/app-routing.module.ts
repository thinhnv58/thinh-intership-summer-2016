import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JobsComponent }      from './job/jobs.component';
import { JobDetailComponent }  from './job/job-detail.component';

import { CustomerDetailComponent } from './customer/customer-detail.component';
import { CustomersComponent } from './customer/customers.component';

import { CarsComponent } from './car/cars.component';
import { CarDetailComponent } from './car/car-detail.component';

const routes: Routes = [
    { path: '', redirectTo: '/jobs', pathMatch: 'full' },
    { path: 'jobs',     component: JobsComponent },
    { path: 'jobs/:id', component: JobDetailComponent },
    { path: 'jobs/create', component: JobDetailComponent },
    { path: 'customers', component: CustomersComponent },
    { path: 'customers/create', component: CustomerDetailComponent },
    { path: 'customers/:id', component: CustomerDetailComponent },
    { path: 'cars', component: CarsComponent },
    { path: 'cars/create', component: CarDetailComponent },
    { path: 'cars/:id', component: CarDetailComponent }
    
    
    
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
