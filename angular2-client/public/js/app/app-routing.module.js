"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var jobs_component_1 = require('./job/jobs.component');
var job_detail_component_1 = require('./job/job-detail.component');
var customer_detail_component_1 = require('./customer/customer-detail.component');
var customers_component_1 = require('./customer/customers.component');
var cars_component_1 = require('./car/cars.component');
var car_detail_component_1 = require('./car/car-detail.component');
var routes = [
    { path: '', redirectTo: '/jobs', pathMatch: 'full' },
    { path: 'jobs', component: jobs_component_1.JobsComponent },
    { path: 'jobs/:id', component: job_detail_component_1.JobDetailComponent },
    { path: 'jobs/create', component: job_detail_component_1.JobDetailComponent },
    { path: 'customers', component: customers_component_1.CustomersComponent },
    { path: 'customers/create', component: customer_detail_component_1.CustomerDetailComponent },
    { path: 'customers/:id', component: customer_detail_component_1.CustomerDetailComponent },
    { path: 'cars', component: cars_component_1.CarsComponent },
    { path: 'cars/create', component: car_detail_component_1.CarDetailComponent },
    { path: 'cars/:id', component: car_detail_component_1.CarDetailComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map