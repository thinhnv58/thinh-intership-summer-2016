export class Product{
    id: number;
    created: string;
    updated: string;
    catalog: number;
    name: string;
    cost: string;
    photo: string;
    quantum: number;
}