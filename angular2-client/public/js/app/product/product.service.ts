import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Product } from './product';
@Injectable()
export class ProductService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/products/.json';  // URL to web api
  constructor(private http: Http) { }
  getProducts(): Promise<Product[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as Product[])
               .catch(this.handleError);
  }
  getProduct(id: number): Promise<Product> {
    return this.getProducts()
               .then(products => products.find(product => product.id === id));
  }

  getProductsByCatalog(catalogid: number): Promise<Product[]> {
    return this.getProducts()
               .then(products => products.filter(product => product.catalog == catalogid));
  }



  delete(id: number): Promise<void> {
    const url = `http://localhost:8000/products/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(product: Product): Promise<Product> {
    return this.http.post('http://localhost:8000/products/', JSON.stringify(product), {headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }
  
  update(product: Product): Promise<Product> {
    const url = `http://localhost:8000/products/${product.id}`;
    return this.http
      .put(url, JSON.stringify(product), {headers: this.headers})
      .toPromise()
      .then(() => product)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
