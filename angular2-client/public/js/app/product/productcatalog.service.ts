import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ProductCatalog } from './productcatalog';
@Injectable()
export class ProductCatalogService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/products/catalog.json';  // URL to web api
  constructor(private http: Http) { }
  getProductCatalogs(): Promise<ProductCatalog[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as ProductCatalog[])
               .catch(this.handleError);
  }
  getProductCatalog(id: number): Promise<ProductCatalog> {
    return this.getProductCatalogs()
               .then(productCatalogs => productCatalogs.find(productCatalog => productCatalog.id === id));
  }

  getProductCatalogsByType(type: string): Promise<ProductCatalog[]> {
    return this.getProductCatalogs()
               .then(productCatalogs => productCatalogs.filter(productCatalog => productCatalog.type === type));
  }
  


  delete(id: number): Promise<void> {
    const url = `http://localhost:8000/products/catalog/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(productCatalog: ProductCatalog): Promise<ProductCatalog> {
    return this.http.post('http://localhost:8000/products/catalog', JSON.stringify(productCatalog), {headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }
  
  update(productCatalog: ProductCatalog): Promise<ProductCatalog> {
    const url = `http://localhost:8000/products/catalog/${productCatalog.id}`;
    return this.http
      .put(url, JSON.stringify(productCatalog), {headers: this.headers})
      .toPromise()
      .then(() => productCatalog)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
