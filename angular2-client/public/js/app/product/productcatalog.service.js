"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var ProductCatalogService = (function () {
    function ProductCatalogService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.url = 'http://localhost:8000/products/catalog.json'; // URL to web api
    }
    ProductCatalogService.prototype.getProductCatalogs = function () {
        return this.http.get(this.url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    ProductCatalogService.prototype.getProductCatalog = function (id) {
        return this.getProductCatalogs()
            .then(function (productCatalogs) { return productCatalogs.find(function (productCatalog) { return productCatalog.id === id; }); });
    };
    ProductCatalogService.prototype.getProductCatalogsByType = function (type) {
        return this.getProductCatalogs()
            .then(function (productCatalogs) { return productCatalogs.filter(function (productCatalog) { return productCatalog.type === type; }); });
    };
    ProductCatalogService.prototype.delete = function (id) {
        var url = "http://localhost:8000/products/catalog/" + id;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    ProductCatalogService.prototype.create = function (productCatalog) {
        return this.http.post('http://localhost:8000/products/catalog', JSON.stringify(productCatalog), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ProductCatalogService.prototype.update = function (productCatalog) {
        var url = "http://localhost:8000/products/catalog/" + productCatalog.id;
        return this.http
            .put(url, JSON.stringify(productCatalog), { headers: this.headers })
            .toPromise()
            .then(function () { return productCatalog; })
            .catch(this.handleError);
    };
    ProductCatalogService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    ProductCatalogService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ProductCatalogService);
    return ProductCatalogService;
}());
exports.ProductCatalogService = ProductCatalogService;
//# sourceMappingURL=productcatalog.service.js.map