export class TeamRepair{
    id: number;
    created: string;
    updated: string;
    name: string;
    description: string;
}
