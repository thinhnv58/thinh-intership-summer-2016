"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var profile_service_1 = require('./profile.service');
var io = require("socket.io-client");
var ProfileComponent = (function () {
    function ProfileComponent(profileService) {
        this.profileService = profileService;
        this.socket = null;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        this.socket = io('http://localhost:3000');
        this.socket.on('profile detail', function (sessionid) {
            var _this = this;
            this.sessionid = sessionid;
            console.log(sessionid);
            this.profileService.getProfile(this.sessionid)
                .then(function (profile) {
                _this.profile = profile;
                console.log(_this.profile);
            });
        });
    };
    ProfileComponent = __decorate([
        core_1.Component({
            selector: 'my-profile',
            templateUrl: 'js/app/user/profile.component.html',
            providers: [profile_service_1.ProfileService]
        }), 
        __metadata('design:paramtypes', [profile_service_1.ProfileService])
    ], ProfileComponent);
    return ProfileComponent;
}());
exports.ProfileComponent = ProfileComponent;
//# sourceMappingURL=profile.component.js.map