import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Member } from './member';
@Injectable()
export class ProfileService {
  private headers = new Headers({'Content-Type': 'application/json'});
  /*private url = 'http://localhost:8000/members/';*/  // URL to web api
  constructor(private http: Http) { }
 
  getProfile(sessionid): Promise<Member> {
      return this.http.get('http://localhost:8000/members/getprofile/' + sessionid)
      .toPromise()
      .then(response => response.json() as Member)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
