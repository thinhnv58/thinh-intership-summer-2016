import { Component, OnInit } from '@angular/core';
import { Member } from './member';
import { ProfileService } from './profile.service';

import * as io from "socket.io-client";

@Component({
  selector: 'my-profile',
  templateUrl: 'js/app/user/profile.component.html',
  providers: [ ProfileService ]
})
export class ProfileComponent implements OnInit{
  profile: Member;
  sessionid: string;
  socket = null;
  constructor(
    private profileService: ProfileService
  ){}


  


  ngOnInit(): void{

    this.socket = io('http://localhost:3000');
    this.socket.on('profile detail', function(sessionid){
      this.sessionid = sessionid;
      console.log(sessionid);

      this.profileService.getProfile(this.sessionid)
        .then(profile => {
            this.profile = profile;
            console.log(this.profile)
      });

    });

  }
}
