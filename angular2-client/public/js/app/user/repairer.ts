export class Repairer{
    id: number;
    user: number;
    name: string;
    type: string;
    avatar: string;
    address: string;
    town_or_city: string;
    phone: string;
    mobile: string;
    note: string;
    team: number;
    is_leader: boolean;
}
