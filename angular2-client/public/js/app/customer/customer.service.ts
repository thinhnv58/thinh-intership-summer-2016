import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Customer } from './customer';
@Injectable()
export class CustomerService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://localhost:8000/customers/.json';  // URL to web api
  constructor(private http: Http) { }
  getCustomers(): Promise<Customer[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(response => response.json() as Customer[])
               .catch(this.handleError);
  }
  getCustomer(id: number): Promise<Customer> {
    return this.getCustomers()
               .then(customers => customers.find(customer => customer.id === id));
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
  
/*  create(first_name: string, last_name: string, isMale: boolean, isCompany: boolean, organization: string, 
  phone: string, mobile: string, email: string, address1: string, address2: string, town_or_city: string, note: string ): Promise<Customer> {
    return this.http
      .post('http://localhost:8000/customers/', JSON.stringify({first_name: first_name, last_name: last_name, isMale: isMale, isCompany: isCompany,
         organization: organization, phone: phone, mobile: mobile, email: email, address1: address1, address2: address2, 
         town_or_city: town_or_city, note: note}), {headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }
*/
  create(customer: Customer): Promise<Customer> {
    return this.http.post('http://localhost:8000/customers/', JSON.stringify(customer), {headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }
  
  update(customer: Customer): Promise<Customer> {
    const url = `http://localhost:8000/customers/${customer.id}`;
    return this.http
      .put(url, JSON.stringify(customer), {headers: this.headers})
      .toPromise()
      .then(() => customer)
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
