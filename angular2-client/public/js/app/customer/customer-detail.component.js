"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var customer_1 = require('./customer');
var customer_service_1 = require('./customer.service');
var CustomerDetailComponent = (function () {
    function CustomerDetailComponent(customerService, route, location) {
        this.customerService = customerService;
        this.route = route;
        this.location = location;
        this.customer = new customer_1.Customer();
    }
    CustomerDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var id = +params['id'];
            if (!isNaN(id)) {
                _this.customerService.getCustomer(id)
                    .then(function (customer) {
                    _this.customer = customer;
                    console.log(_this.customer.first_name);
                });
            }
        });
    };
    CustomerDetailComponent.prototype.updateCustomer = function () {
        var _this = this;
        this.customerService.update(this.customer)
            .then(function () { return _this.goBack(); });
    };
    /*  createCustomer(first_name, last_name, isMale, isCompany, organization, phone,
      mobile, email, address1, address2, town_or_city, note): void{
        this.customerService.create(first_name, last_name, isMale, isCompany, organization, phone,
      mobile, email, address1, address2, town_or_city, note);
        this.goBack();
      }*/
    CustomerDetailComponent.prototype.createCustomer = function () {
        this.customerService.create(this.customer);
    };
    CustomerDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    CustomerDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-hero-detail',
            templateUrl: 'js/app/customer/customer-detail.component.html',
        }), 
        __metadata('design:paramtypes', [customer_service_1.CustomerService, router_1.ActivatedRoute, common_1.Location])
    ], CustomerDetailComponent);
    return CustomerDetailComponent;
}());
exports.CustomerDetailComponent = CustomerDetailComponent;
//# sourceMappingURL=customer-detail.component.js.map