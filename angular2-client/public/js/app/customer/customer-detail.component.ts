import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location }               from '@angular/common';
import { Customer }        from './customer';
import { CustomerService } from './customer.service';
@Component({
  selector: 'my-hero-detail',
  templateUrl: 'js/app/customer/customer-detail.component.html',
})
export class CustomerDetailComponent implements OnInit {
  customer = new Customer();
  

  constructor(
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      if ( !isNaN(id)){
        this.customerService.getCustomer(id)
          .then(customer => {
            this.customer = customer;
            console.log(this.customer.first_name);
          });
      }
        
    });
  }

  updateCustomer(): void {
    this.customerService.update(this.customer)
      .then(() => this.goBack());
  }

/*  createCustomer(first_name, last_name, isMale, isCompany, organization, phone,
  mobile, email, address1, address2, town_or_city, note): void{
    this.customerService.create(first_name, last_name, isMale, isCompany, organization, phone,
  mobile, email, address1, address2, town_or_city, note);
    this.goBack();
  }*/
  createCustomer(): void{
    this.customerService.create(this.customer);
    
  }



  goBack(): void {
    this.location.back();
  }
}
