"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var CustomerService = (function () {
    function CustomerService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.url = 'http://localhost:8000/customers/.json'; // URL to web api
    }
    CustomerService.prototype.getCustomers = function () {
        return this.http.get(this.url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    CustomerService.prototype.getCustomer = function (id) {
        return this.getCustomers()
            .then(function (customers) { return customers.find(function (customer) { return customer.id === id; }); });
    };
    CustomerService.prototype.delete = function (id) {
        var url = this.url + "/" + id;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    /*  create(first_name: string, last_name: string, isMale: boolean, isCompany: boolean, organization: string,
      phone: string, mobile: string, email: string, address1: string, address2: string, town_or_city: string, note: string ): Promise<Customer> {
        return this.http
          .post('http://localhost:8000/customers/', JSON.stringify({first_name: first_name, last_name: last_name, isMale: isMale, isCompany: isCompany,
             organization: organization, phone: phone, mobile: mobile, email: email, address1: address1, address2: address2,
             town_or_city: town_or_city, note: note}), {headers: this.headers})
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
      }
    */
    CustomerService.prototype.create = function (customer) {
        return this.http.post('http://localhost:8000/customers/', JSON.stringify(customer), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    CustomerService.prototype.update = function (customer) {
        var url = "http://localhost:8000/customers/" + customer.id;
        return this.http
            .put(url, JSON.stringify(customer), { headers: this.headers })
            .toPromise()
            .then(function () { return customer; })
            .catch(this.handleError);
    };
    CustomerService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    CustomerService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], CustomerService);
    return CustomerService;
}());
exports.CustomerService = CustomerService;
//# sourceMappingURL=customer.service.js.map