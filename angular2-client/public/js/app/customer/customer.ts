export class Customer {
    id: number;
    created: string;
    updated: string;
    first_name: string;
    last_name: string;
    isMale: boolean;
    isCompany: boolean;
    organization: string;
    phone: string;
    mobile: string;
    email: string;
    address1: string;
    address2: string;
    town_or_city: string;
    country: string;
    note: string;


}


