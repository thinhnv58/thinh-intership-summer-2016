import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { Customer } from './customer';
@Injectable()
export class CustomerSearchService {
  constructor(private http: Http) {}
  search(term: string): Observable<Customer[]> {
    return this.http
               .get(`http://localhost:8000/customers/?search=${term}`)
               .map((r: Response) => r.json() as Customer[]);
  }
}
