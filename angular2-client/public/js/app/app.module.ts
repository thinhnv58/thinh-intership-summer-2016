import './rxjs-extensions';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent }   from './app.component';
import { JobsComponent } from './job/jobs.component';
import { JobDetailComponent } from './job/job-detail.component';
import { JobService } from './job/job.service';

import { CustomerDetailComponent } from './customer/customer-detail.component';
import { CustomerService } from './customer/customer.service';
import { CustomersComponent } from './customer/customers.component';

import { CarService } from './car/car.service';
import { CarDetailComponent } from './car/car-detail.component';
import { CarsComponent } from './car/cars.component';

import { ProfileComponent } from './user/profile.component';

import { AppRoutingModule }     from './app-routing.module';

import { CookieService } from 'angular2-cookie/services/cookies.service';

@NgModule({
  imports:      [ 
    BrowserModule,
    HttpModule, 
    AppRoutingModule,
    FormsModule,
     ],
  declarations: [ 
    AppComponent,  
    JobsComponent, 
    JobDetailComponent,
    CustomerDetailComponent,
    CustomersComponent,
    CarsComponent, 
    CarDetailComponent,
    ProfileComponent
    ],
  bootstrap:    [ AppComponent ],
  providers: [ 
    JobService,
    CustomerService, 
    CarService,
    CookieService,
    
  ]
  
})
export class AppModule { }
