var express = require('express');
var path = require('path');
var cookieParser = require('socket.io-cookie');

var app = express();
var http = require('http');
var server = http.Server(app);
var io = require('socket.io')(server);
io.use(cookieParser);
var querystring = require('querystring');
var routes = require('./routes/index');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
 
var redis = require('redis');
var sub = redis.createClient();
sub.subscribe('chat');




app.use(express.static(path.join(__dirname, 'public')));
app.use('/node_modules', express.static(__dirname + '/node_modules'));


app.use('/*', routes);

io.on('connection', function(socket){
    socket.emit('profile detail', socket.request.headers.cookie.sessionid , function (data) {
      console.log(data); // data will be 'woot'
    });

  /*sub.on('message', function(channel, msg){
    socket.send(msg);
  });
  console.log('a user connected');
  socket.on('send message', function(msg){
    console.log(msg);
    var request = require("request");
    request({
      uri: "http://localhost:8000/comments/",
      method: "POST",
      form: {
        text: msg
      }
    }, function(error, response, body) {
      console.log(body);
    });

  })*/
});






module.exports = {app: app, server: server};