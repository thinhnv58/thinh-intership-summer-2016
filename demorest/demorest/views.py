from django.shortcuts import render
from members.models import Member

def home(request):

    member = Member.objects.get(pk=1)
    return render(request, 'index.html', {
        'member': member
        })
    