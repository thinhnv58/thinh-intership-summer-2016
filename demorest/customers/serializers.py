from rest_framework import serializers

from .models import Customer
class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('id', 'created', 'updated', 'first_name', 'last_name', 'isMale', 'isCompany', 
            'organization', 'phone', 'mobile', 'email', 'address1', 'address2', 'town_or_city', 'country', 'note')
        





