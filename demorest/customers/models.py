from django.db import models

class Customer(models.Model):
    COUNTRY_CHOICES = [
        ('VN', 'Viet Nam'),
        ('US', 'United States')

    ]
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    isMale = models.BooleanField(default=True)
    isCompany = models.BooleanField(default=False)
    organization = models.CharField(null=True, blank=True, max_length=255)
    
    phone = models.CharField(null=True, blank=True, max_length=16)
    mobile = models.CharField(null=True, blank=True, max_length=16)
    email = models.EmailField(null=True, blank=True)
    address1 = models.CharField(null=True, blank=True, max_length=255)
    address2 = models.CharField(null=True, blank=True, max_length=255)
    town_or_city = models.CharField(null=True, blank=True, max_length=255)
    country = models.CharField(choices=COUNTRY_CHOICES ,default='VN', max_length=100)
    note = models.TextField(null=True, blank=True)


    def __str__(self):
        return (self.first_name + ' ' + self.last_name)




