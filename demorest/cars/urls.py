from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^carmakes$', views.CarMakeList.as_view()),
    url(r'^carmakes/(?P<pk>[0-9]+)$', views.CarMakeDetail.as_view()),
    url(r'^carmodels$', views.CarModelList.as_view()),
    url(r'^carmodels/(?P<pk>[0-9]+)$', views.CarModelDetail.as_view()),
    
    url(r'^modellistbymake/(?P<pk>[0-9]+)$', views.model_list_by_make),
    
    url(r'^$', views.CarList.as_view()),
    url(r'^(?P<pk>[0-9]+)$', views.CarDetail.as_view()),
    

    
]

urlpatterns = format_suffix_patterns(urlpatterns)
