from rest_framework import serializers

from .models import Car, CarModel, CarMake

class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('id', 'created', 'updated', 'license_plate', 'carModel', 
        'year', 'vin', 'horse_power', 'fuel', 'gear_box', 'engine', 'milekm_age', 'note')
        


class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarModel
        fields = ('id', 'name', 'make', 'type', 'photo', 'description')
        
class CarMakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarMake
        fields = ('id', 'name')
        