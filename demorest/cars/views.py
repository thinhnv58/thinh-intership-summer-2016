from .models import Car, CarModel, CarMake
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser


from rest_framework import generics
from rest_framework import filters

from .serializers import CarSerializer, CarModelSerializer, CarMakeSerializer



class CarList(generics.ListCreateAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('^license_plate', '^note')
    
class CarDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer


class CarModelList(generics.ListCreateAPIView):
    queryset = CarModel.objects.all()
    serializer_class = CarModelSerializer
    
    
class CarModelDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CarModel.objects.all()
    serializer_class = CarModelSerializer



class CarMakeList(generics.ListCreateAPIView):
    queryset = CarMake.objects.all()
    serializer_class = CarMakeSerializer
    
    
class CarMakeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = CarMake.objects.all()
    serializer_class = CarMakeSerializer


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def model_list_by_make (request, pk):
    """
    get list model by car make
    """
    try:
        carmake = CarMake.objects.get(pk=pk)
    except CarMake.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        carmodels = CarModel.objects.filter(make=carmake)
        serializer = CarModelSerializer(carmodels, many=True)
        return JSONResponse(serializer.data)
