from django.db import models

class CarMake(models.Model):
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name


class CarModel(models.Model):
    name = models.CharField(max_length=100)
    make = models.ForeignKey(CarMake, related_name='carmodels')
    type = models.CharField(null=True, blank=True, max_length=255)
    photo = models.CharField(null=True, blank=True, max_length=255)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name



class Car(models.Model):
    FUEL_CHOICES = [
        ('P' , 'Petrol'),
        ('D' , 'Diesel'),
        ('E' , 'Electric'),
        ('H' , 'Hybrid'),
        ('L' , 'LPG'),
        ('O' , 'Other')
    ] 

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    license_plate = models.CharField(max_length=30)
    carModel = models.ForeignKey(CarModel, related_name='cars')
    year = models.CharField(null=True, blank=True, max_length=16)
    
    vin = models.CharField(null=True, blank=True, max_length=50)
    horse_power = models.CharField(null=True, blank=True, max_length=50)
    fuel = models.CharField(choices=FUEL_CHOICES ,default='P', max_length=10)
    gear_box = models.CharField(null=True, blank=True, max_length=50)
    engine = models.CharField(null=True, blank=True, max_length=50)

    milekm_age = models.CharField(null=True, blank=True, max_length=50)
    note = models.TextField(null=True, blank=True)

    def __str__(self):
        return (self.license_plate + ' ' + self.carModel.name + '-' + self.carModel.make.name)
    
    
