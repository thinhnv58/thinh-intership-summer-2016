from rest_framework import serializers

from .models import Job, JobService, JobProduct, JobMessage, JobEvent
class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ('id', 'created', 'updated', 'customer', 'car', 'problems', 'due_from', 
            'due_to', 'cost', 'draft', 'advisor', 'note', 'team_repair', 'status')


class JobServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobService
        fields = ('id', 'name', 'cost', 'job')


class JobProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobProduct
        fields = ('id', 'job', 'product', 'quantum')

class JobMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobMessage
        fields = ('id', 'created', 'updated', 'author', 'text', 'job')

class JobEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobEvent
        fields = ('id', 'created', 'updated', 'text', 'job')
