from .models import Job, JobProduct, JobService, JobMessage, JobEvent
from .serializers import JobSerializer, JobProductSerializer, JobServiceSerializer, JobMessageSerializer, JobEventSerializer
from rest_framework import generics


class JobList(generics.ListCreateAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    
class JobDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer




class JobProductList(generics.ListCreateAPIView):
    queryset = JobProduct.objects.all()
    serializer_class = JobProductSerializer
    
class JobProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = JobProduct.objects.all()
    serializer_class = JobProductSerializer




class JobServiceList(generics.ListCreateAPIView):
    queryset = JobService.objects.all()
    serializer_class = JobServiceSerializer
    
class JobServiceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = JobService.objects.all()
    serializer_class = JobServiceSerializer





class JobMessageList(generics.ListCreateAPIView):
    queryset = JobMessage.objects.all()
    serializer_class = JobMessageSerializer
    
class JobMessageDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = JobMessage.objects.all()
    serializer_class = JobMessageSerializer





class JobEventList(generics.ListCreateAPIView):
    queryset = JobEvent.objects.all()
    serializer_class = JobEventSerializer
    
class JobEventDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = JobEvent.objects.all()
    serializer_class = JobEventSerializer




