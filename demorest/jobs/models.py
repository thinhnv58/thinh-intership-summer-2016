import datetime
from django.db import models
from customers.models import Customer 
from cars.models import Car
from members.models import Advisor, TeamRepair, Member
from services.models import Service
from products.models import Product


class Job(models.Model):
    STATUS_CHOICES = [
        ('W' , 'Waiting'),
        ('I' , 'Inprocess'),
        ('P' , 'Pause'),
        ('C' , 'Completed')
    ] 

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    customer = models.ForeignKey(Customer, related_name='customer_jobs')
    car = models.ForeignKey(Car, related_name='car_jobs')
    problems = models.TextField(null=True, blank=True)
    due_from = models.DateField()
    due_to = models.DateField()
    cost = models.CharField(blank=True, null=True, max_length=20)
    draft = models.BooleanField(default=True, blank=True)
    advisor = models.ForeignKey(Advisor, related_name='advisor_jobs', null=True, blank=True, )
    note = models.TextField(null=True, blank=True)

    team_repair = models.ForeignKey(TeamRepair, related_name='repair_jobs')
    status = models.CharField(choices=STATUS_CHOICES ,default='W', max_length=10)


    def __str__(self):
        return (self.customer.first_name + self.car.license_plate)

class JobService(Service):
    job = models.ForeignKey(Job, related_name='job_services')
    def __str__(self):
        return self.name


class JobProduct(models.Model):
    job = models.ForeignKey(Job, related_name="job_products")
    product = models.ForeignKey(Product)
    quantum = models.IntegerField(blank=True, null=True)
    
    def __str__(self):
        return self.product.name

class JobMessage(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True) 
    author = models.ForeignKey(Member, related_name='author_messages')
    text = models.TextField()
    job = models.ForeignKey(Job, related_name='job_messages')
    def __str__(self):
        return self.text
    

class JobEvent(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True) 
    text = models.TextField()
    job = models.ForeignKey(Job, related_name='job_events')
    def __str__(self):
        return self.text
    





'''
class JobEX(models.Model):
    STATUS_CHOICES = [
        ('W' , 'Waiting'),
        ('I' , 'Inprocess'),
        ('P' , 'Pause'),
        ('C' , 'Completed')
    ] 

    job = models.ForeignKey(Job)
    team_repair = models.ForeignKey(TeamRepair, related_name='repair_jobs')
    status = models.CharField(choices=STATUS_CHOICES ,default='W', max_length=10)

    def __str__(self):
        return (self.job.customer.name + self.job.car.license_plate)
class ExService(Service):
    job = models.ForeignKey(JobEX, related_name='ex_services')
    def __str__(self):
        return self.name
class ExProduct(models.Model):
    job = models.ForeignKey(JobEX, related_name="ex_products")
    product = models.ForeignKey(Product)
    quantum = models.IntegerField(blank=True, null=True)
    def __str__(self):
        return self.product.name

'''

    

