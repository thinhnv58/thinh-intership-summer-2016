from django.contrib import admin

from .models import Job, JobProduct, JobService, JobMessage, JobEvent
admin.site.register(Job)

admin.site.register(JobProduct)
admin.site.register(JobService)
admin.site.register(JobMessage)
admin.site.register(JobEvent)
