from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    #url(r'^$', views.job_list),
    #url(r'^(?P<pk>[0-9]+)$', views.job_detail)
    url(r'^$', views.JobList.as_view()),
    url(r'^(?P<pk>[0-9]+)$', views.JobDetail.as_view()),
    
    url(r'^jobservices$', views.JobServiceList.as_view()),
    url(r'^jobservices/(?P<pk>[0-9]+)$', views.JobServiceDetail.as_view()),
    
    url(r'^jobproducts$', views.JobProductList.as_view()),
    url(r'^jobproducts/(?P<pk>[0-9]+)$', views.JobProductDetail.as_view()),
    
    url(r'^jobevents$', views.JobEventList.as_view()),
    url(r'^jobevents/(?P<pk>[0-9]+)$', views.JobEventDetail.as_view()),
    
    url(r'^jobmessages$', views.JobMessageList.as_view()),
    url(r'^jobmessages/(?P<pk>[0-9]+)$', views.JobMessageDetail.as_view()),
    
]
urlpatterns = format_suffix_patterns(urlpatterns)