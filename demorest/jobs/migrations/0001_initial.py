# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-10-23 06:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cars', '0005_auto_20161018_0127'),
        ('members', '0004_repairer_is_leader'),
        ('services', '0001_initial'),
        ('products', '0001_initial'),
        ('customers', '0002_auto_20161017_0449'),
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('problems', models.TextField(blank=True, null=True)),
                ('due_from', models.DateField()),
                ('due_to', models.DateField()),
                ('es_cost', models.CharField(blank=True, max_length=20, null=True)),
                ('draft', models.BooleanField(default=True)),
                ('note', models.TextField(blank=True, null=True)),
                ('status', models.CharField(choices=[('W', 'Waiting'), ('I', 'Inprocess'), ('P', 'Pause'), ('C', 'Completed')], default='W', max_length=10)),
                ('advisor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='advisor_jobs', to='members.Advisor')),
                ('car', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='car_jobs', to='cars.Car')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='customer_jobs', to='customers.Customer')),
                ('team_repair', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='repair_jobs', to='members.TeamRepair')),
            ],
        ),
        migrations.CreateModel(
            name='JobEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('text', models.TextField()),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='job_events', to='jobs.Job')),
            ],
        ),
        migrations.CreateModel(
            name='JobMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('text', models.TextField()),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='author_messages', to='members.Member')),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='job_messages', to='jobs.Job')),
            ],
        ),
        migrations.CreateModel(
            name='JobProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantum', models.IntegerField(blank=True, null=True)),
                ('cost', models.CharField(blank=True, max_length=20, null=True)),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='job_products', to='jobs.Job')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='products.Product')),
            ],
        ),
        migrations.CreateModel(
            name='JobService',
            fields=[
                ('service_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='services.Service')),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='job_services', to='jobs.Job')),
            ],
            bases=('services.service',),
        ),
    ]
