from django.db import models

class Service(models.Model):
    name = models.TextField()
    cost = models.CharField(max_length=20)
    def __str__(self):
        return self.name
    
class MainService(Service):
    def __str__(self):
        return self.name


