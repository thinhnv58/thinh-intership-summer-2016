from .models import MainService
from .serializers import MainServiceSerializer
from rest_framework import generics


class MainServiceList(generics.ListCreateAPIView):
    queryset = MainService.objects.all()
    serializer_class = MainServiceSerializer
