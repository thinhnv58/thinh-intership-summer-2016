from rest_framework import serializers

from .models import MainService
class MainServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = MainService
        fields = ('id', 'name', 'cost')
        

