from django.contrib import admin

from .models import Service, MainService

admin.site.register(Service)
admin.site.register(MainService)
