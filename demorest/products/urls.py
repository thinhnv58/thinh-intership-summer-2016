from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^catalog$', views.ProductCatalogList.as_view()),
    url(r'^catalog/(?P<pk>[0-9]+)$', views.ProductCatalogDetail.as_view()),

    url(r'^$', views.ProductList.as_view()),
    url(r'^(?P<pk>[0-9]+)$', views.ProductDetail.as_view()),

    
]

urlpatterns = format_suffix_patterns(urlpatterns)
