from django.shortcuts import render
from rest_framework import generics
from .models import ProductCatalog, Product
from .serializers import ProductSerializer, ProductCatalogSerializer

class ProductCatalogList(generics.ListCreateAPIView):
    queryset = ProductCatalog.objects.all()
    serializer_class = ProductCatalogSerializer
    
class ProductCatalogDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductCatalog.objects.all()
    serializer_class = ProductCatalogSerializer


class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    
class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


