from django.db import models

class ProductCatalog(models.Model):
    TYPE_CHOICES = [
        ('P' , 'Performance and Parts'),
        ('E' , 'Exterior Acccessories'),
        ('I' , 'Interior Acccessories'),
        ('O' , 'Other Acccessories')
    ] 
    type = models.CharField(choices=TYPE_CHOICES ,default='P', max_length=100)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Product(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    catalog = models.ForeignKey(ProductCatalog, related_name='products')
    name = models.CharField(max_length=255)
    cost = models.CharField(null=True, blank=True, max_length=20)
    photo = models.CharField(null=True, blank=True, max_length=500)
    quantum = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name


    