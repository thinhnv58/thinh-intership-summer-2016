from rest_framework import serializers

from .models import Product, ProductCatalog


class ProductCatalogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCatalog
        fields = ('id', 'type', 'name')
        

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'created', 'updated', 'catalog', 'name', 'cost', 'photo', 'quantum')
