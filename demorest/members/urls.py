from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    url(r'^$', views.MemberList.as_view()),
    url(r'^(?P<pk>[0-9]+)$', views.MemberDetail.as_view()),
    url(r'^getprofile/(?P<sessionid>[\w-]+)$', views.get_profile),
    url(r'^teamrepairs$', views.TeamRepairList.as_view()),
    url(r'^teamrepairs(?P<pk>[0-9]+)$', views.TeamRepairDetail.as_view()),

    url(r'^repairers$', views.RepairerList.as_view()),
    url(r'^repairers(?P<pk>[0-9]+)$', views.RepairerDetail.as_view()),

    
]

urlpatterns = format_suffix_patterns(urlpatterns)
