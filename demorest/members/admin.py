from django.contrib import admin

from .models import Member, Repairer, Advisor, Manager, TeamRepair

admin.site.register(Member)
admin.site.register(Repairer)
admin.site.register(Advisor)
admin.site.register(Manager)
admin.site.register(TeamRepair)
