from django.db import models
from django.contrib.auth.models import User

class Member(models.Model):
    user = models.ForeignKey(User)
    TYPE_CHOICES = [
        ('M' , 'Manager'),
        ('A' , 'Advisor'),
        ('R' , 'Repairer')
    ]
    type = models.CharField(choices=TYPE_CHOICES ,default='R', max_length=10)
    name = models.CharField(null=True, blank=True, max_length=100)
    avatar = models.ImageField(null=True, blank=True, upload_to="avatars/")
    address = models.CharField(null=True, blank=True, max_length=255)
    town_or_city = models.CharField(null=True, blank=True, max_length=50)
    phone = models.CharField(null=True, blank=True, max_length=20)
    mobile = models.CharField(null=True, blank=True, max_length=20)
    note = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.user.username
    
class TeamRepair(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    def __str__(self):
        return self.name 

class Advisor(Member):
    def __str__(self):
        return self.user.username
class Manager(Member):
    def __str__(self):
        return self.user.username
class Repairer(Member):
    team = models.ForeignKey(TeamRepair, related_name="repairs")
    is_leader = models.BooleanField(default=False)
    def __str__(self):
        return self.user.username
    