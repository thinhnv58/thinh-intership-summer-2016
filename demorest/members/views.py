from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework import generics

import redis
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User

from .models import Member, TeamRepair, Repairer
from .serializers import MemberSerializer, TeamRepairSerializer, RepairerSerializer


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)




@csrf_exempt
def get_profile(request, sessionid):
    try:
        session = Session.objects.get(session_key=sessionid)
        user_id = session.get_decoded().get('_auth_user_id')
        user = User.objects.get(id=user_id)
        member = Member.objects.get(user=user)
    except Member.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = MemberSerializer(member)
        return JSONResponse(serializer.data)



class MemberList(generics.ListCreateAPIView):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer
    
class MemberDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer


class TeamRepairList(generics.ListCreateAPIView):
    queryset = TeamRepair.objects.all()
    serializer_class = TeamRepairSerializer
    
class TeamRepairDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = TeamRepair.objects.all()
    serializer_class = TeamRepairSerializer

class RepairerList(generics.ListCreateAPIView):
    queryset = Repairer.objects.all()
    serializer_class = RepairerSerializer
    
class RepairerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Repairer.objects.all()
    serializer_class = RepairerSerializer





