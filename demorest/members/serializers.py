from rest_framework import serializers

from .models import Member, TeamRepair, Repairer
class MemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = ('id', 'user', 'name', 'type', 'avatar', 'address', 'town_or_city', 'phone', 'mobile', 'note')
        

class TeamRepairSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamRepair
        fields = ('id', 'created', 'updated', 'name', 'description')
        

class RepairerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Repairer
        fields = ('id', 'user', 'name', 'type', 'avatar', 'address', 'town_or_city', 'phone', 'mobile', 'note', 'team', 'is_leader')
        



