# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-10-18 08:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0003_auto_20161018_0835'),
    ]

    operations = [
        migrations.AddField(
            model_name='repairer',
            name='is_leader',
            field=models.BooleanField(default=False),
        ),
    ]
