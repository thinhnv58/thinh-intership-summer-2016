# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-10-18 08:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_advisor'),
    ]

    operations = [
        migrations.CreateModel(
            name='Manager',
            fields=[
                ('member_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='members.Member')),
            ],
            bases=('members.member',),
        ),
        migrations.CreateModel(
            name='Repairer',
            fields=[
                ('member_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='members.Member')),
            ],
            bases=('members.member',),
        ),
        migrations.CreateModel(
            name='TeamRepair',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='repairer',
            name='team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='repairs', to='members.TeamRepair'),
        ),
    ]
